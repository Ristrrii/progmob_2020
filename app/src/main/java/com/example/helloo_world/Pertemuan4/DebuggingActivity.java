package com.example.helloo_world.Pertemuan4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.helloo_world.Adapter.MahasiswaRecyclerAdapter;
import com.example.helloo_world.Pertemuan2.RecyclerActivity;
import com.example.helloo_world.R;
import com.example.helloo_world.model.Mahasiswa;

import java.util.ArrayList;
import java.util.List;

public class DebuggingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView rv = ( RecyclerView)findViewById(R.id.rvLatihan);
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;

        //data dumy
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        /*Mahasiswa m1 = new Mahasiswa("Ristri","72180204","0895359097066");
        Mahasiswa m2 = new Mahasiswa("Elza","72180214","0895359097077");
        Mahasiswa m3 = new Mahasiswa("Nabia","72180215","0895359097066");
        Mahasiswa m4 = new Mahasiswa("Ribka","72180246","0895359097066");
        Mahasiswa m5 = new Mahasiswa("Billi","72180266","0895359097066");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);


        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(DebuggingActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);


        rv.setLayoutManager(new LinearLayoutManager(DebuggingActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);*/
    }
}