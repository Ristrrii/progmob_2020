package com.example.helloo_world.CrudMatkul;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helloo_world.Adapter.MatkulCRUDRecyclerAdapter;
import com.example.helloo_world.Network.GetDataService;
import com.example.helloo_world.Network.RetrofitClientInstance;
import com.example.helloo_world.R;
import com.example.helloo_world.model.Matkul;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DaftarMatkulActivity extends AppCompatActivity {
    RecyclerView rvMatkul;
    MatkulCRUDRecyclerAdapter matkulAdapter;
    ProgressDialog pd;
    List<Matkul> matakuliahList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_matkul);
        rvMatkul = (RecyclerView)findViewById(R.id.rvMatkul);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Tunggu");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Matkul>> call = service.getMatakuliah("72180204");

        call.enqueue(new Callback<List<Matkul>>() {
            @Override
            public void onResponse(Call<List<Matkul>> call, Response<List<Matkul>> response) {
                pd.dismiss();
                matakuliahList = response.body();
                matkulAdapter = new MatkulCRUDRecyclerAdapter(matakuliahList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DaftarMatkulActivity.this);
                rvMatkul.setLayoutManager(layoutManager);
                rvMatkul.setAdapter(matkulAdapter);

            }

            @Override
            public void onFailure(Call<List<Matkul>> call, Throwable t) {
                Toast.makeText(DaftarMatkulActivity.this,"ERROR", Toast.LENGTH_LONG);
            }
        });
    }
}