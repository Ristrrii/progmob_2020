package com.example.helloo_world.CrudMatkul;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.helloo_world.Network.GetDataService;
import com.example.helloo_world.Network.RetrofitClientInstance;
import com.example.helloo_world.R;
import com.example.helloo_world.model.DefaultResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HapusMatkulActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hapus_matkul);
        EditText edKodeHapus = (EditText)findViewById(R.id.TxtKodeHapus);
        Button btnSimpanHapus = (Button)findViewById(R.id.btnSimpanHapusMatkul);
        pd = new ProgressDialog(this);

        btnSimpanHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Tunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_matkul(edKodeHapus.getText().toString(),"72180204");

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(HapusMatkulActivity.this,"BERHASIL DIHAPUS", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(HapusMatkulActivity.this,"GAGAL DIHAPUS", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}