package com.example.helloo_world.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helloo_world.R;
import com.example.helloo_world.model.Mahasiswa;

import java.util.ArrayList;
import java.util.List;

public class MahasiswaCRUDReyclerAdapter extends RecyclerView.Adapter<MahasiswaCRUDReyclerAdapter.ViewHolder> {
    private Context context;
    private List<Mahasiswa> mahasiswaList;

    public MahasiswaCRUDReyclerAdapter(Context context) {
        this.context = context;
        mahasiswaList = new ArrayList<>();

    }

    public MahasiswaCRUDReyclerAdapter(List<Mahasiswa>mahasiswaList){
        this.mahasiswaList=mahasiswaList;
    }

     public void setMahasiswaList(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
        notifyDataSetChanged();
    }

    public List<Mahasiswa> getMahasiswaList() {
        return mahasiswaList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Mahasiswa m = mahasiswaList.get(position);

        holder.tvNama.setText(m.getNama());
        //holder.tvnotelp.setText(m.getNoTelp());
        holder.tvNim.setText(m.getNim());
        holder.tvAlamat.setText(m.getAlamat());
        holder.tvEmail.setText(m.getEmail());

    }

    @Override
    public int getItemCount() {
        return mahasiswaList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNama,tvNim,tvnotelp, tvAlamat,tvEmail,tvFoto;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvNim = itemView.findViewById(R.id.tvNim);
            // tvnotelp = itemView.findViewById(R.id.tvtNotelpon);
            tvAlamat = itemView.findViewById(R.id.tvAlamat);
            tvEmail = itemView.findViewById(R.id.tvEmail);

        }
    }
}

