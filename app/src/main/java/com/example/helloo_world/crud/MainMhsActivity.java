package com.example.helloo_world.crud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


import com.example.helloo_world.R;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainMhsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mhs);
        //variabel
        Button buttonGetMhs = (Button) findViewById(R.id.buttonGetMhs);
        Button buttonAddMhs = (Button) findViewById(R.id.buttonAddMhs);
        Button buttonDeleteMhs = (Button) findViewById(R.id.buttonDeleteMhs);
        Button buttonUpdateMhs = (Button) findViewById(R.id.buttonUpdate);

        buttonGetMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent = new Intent(MainMhsActivity.this,MahasiswaGetAllActivity.class);
               startActivity(intent);
            }
        });

        buttonAddMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this,MahasiswaAddActivity.class);
                startActivity(intent);
            }
        });

        buttonDeleteMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this,MahasiswaDeleteActivity.class);
                startActivity(intent);
            }
        });

        buttonUpdateMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this,MahasiswaUpdateActivity.class);
                startActivity(intent);
            }
        });
    }
}