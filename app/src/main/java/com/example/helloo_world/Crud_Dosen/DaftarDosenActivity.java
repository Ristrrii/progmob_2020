package com.example.helloo_world.Crud_Dosen;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.helloo_world.Adapter.DosenCRUDRecyclerAdapter;
import com.example.helloo_world.Network.GetDataService;
import com.example.helloo_world.Network.RetrofitClientInstance;
import com.example.helloo_world.R;
import com.example.helloo_world.model.Dosen;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarDosenActivity extends AppCompatActivity {
    RecyclerView rvDosen;
    DosenCRUDRecyclerAdapter dosenAdapter;
    ProgressDialog pd;
    List<Dosen> dosenList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_dosen);

        rvDosen = (RecyclerView)findViewById(R.id.rvDosen);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Tunggu");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Dosen>> call = service.getDosen("72180204");

        call.enqueue(new Callback<List<Dosen>>() {
            @Override
            public void onResponse(Call<List<Dosen>> call, Response<List<Dosen>> response) {
                pd.dismiss();
                dosenList = response.body();
                dosenAdapter = new DosenCRUDRecyclerAdapter(dosenList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DaftarDosenActivity.this);
                rvDosen.setLayoutManager(layoutManager);
                rvDosen.setAdapter(dosenAdapter);
            }

            @Override
            public void onFailure(Call<List<Dosen>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(DaftarDosenActivity.this,"Error",Toast.LENGTH_LONG).show();

            }
        });

    }
}